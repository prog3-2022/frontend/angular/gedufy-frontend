import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Crearpersonarequestmodel} from "../../interfaces/crearpersonarequestmodel.interface";
import {AuthService} from "../../services/auth.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    dni: [undefined, [Validators.required, Validators.minLength(6)]],
    nombre: [undefined, [Validators.required, Validators.minLength(3)]],
    email: [undefined, [Validators.required, Validators.email]],
    tipoPersona: [undefined, Validators.required]
  });

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.miFormulario.reset({});
    this.miFormulario.untouched;
  }

  guardar() {

    if (this.miFormulario.invalid) {
      this.miFormulario.markAllAsTouched();
      return;
    }

    const persona: Crearpersonarequestmodel = {
      dni: this.miFormulario.value.dni,
      nombre: this.miFormulario.value.nombre,
      email: this.miFormulario.value.email,
      tipo_persona_id: this.miFormulario.value.tipoPersona
    };
    this.authService.guardarPersona(persona).subscribe(resp => {
      this.showSnackBar(`Se creo la persona`)
    }, error => {
      this.showSnackBar(`Error al crear persona`)
    });

    this.miFormulario.reset({});
    this.miFormulario.get('dni')!.setErrors(null);
    this.miFormulario.get('nombre')!.setErrors(null);
    this.miFormulario.get('email')!.setErrors(null);
    this.miFormulario.get('tipoPersona')!.setErrors(null);
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, 'Aceptar', {
      duration: 3000
    });
  }
}
