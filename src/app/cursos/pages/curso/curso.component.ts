import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Curso} from "../../interfaces/curso.interface";
import {CursosService} from "../../services/cursos.service";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.scss']
})
export class CursoComponent implements OnInit {

  curso!: Curso;

  constructor(private activatedRoute: ActivatedRoute,
              private cursosService: CursosService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        switchMap(({id}) => this.cursosService.getCursoById(id))
      ).subscribe(resp => this.curso = resp);
  }

  volver() {
    this.router.navigate(['/cursos/listado']);
  }
}
